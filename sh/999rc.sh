alias ls="ls --color"
alias l="ls -lFh"
alias ll="l -a"

alias cls="clear; l"

alias tmux="tmux -2"

# Create shortcuts for parent directories.
# Modify by modifiying the seq parameters in the for loop.
for i in $(seq 2 20); do
    alias $(printf '.%.0s' $(seq 1 $i))="cd $(printf '../%.0s' $(seq 2 $i))"
done

export WORKON_HOME=~/.venvs
export TERM=xterm-256color

