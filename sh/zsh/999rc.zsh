antigen use oh-my-zsh
antigen theme denysdovhan/spaceship-zsh-theme spaceship
antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle git
antigen bundle git-extras

antigen bundle vi-mode

antigen bundle pip
antigen bundle pep8
antigen bundle python
antigen bundle virtualenvwrapper

antigen bundle ruby
antigen bundle rails
antigen bundle bundler
antigen bundle gem

antigen apply
