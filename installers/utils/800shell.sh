# This function creates a startup script
# for the shell that is to be configured.
init_shell() {
    echo > $1

    echo "for f in $DIR/sh/pre/*.*sh; do source \$f; done" >> $1
    echo "for f in $DIR/sh/$2/pre/*.*sh; do source \$f; done" >> $1

    # External things.
    echo "for f in $DIR/sh/lib/*.*sh; do source \$f; done" >> $1
    echo "for f in $DIR/sh/$2/lib/*.*sh; do source \$f; done" >> $1

    # Actual configuration files.
    echo "for f in $DIR/sh/*.*sh; do source \$f; done" >> $1
    echo "for f in $DIR/sh/$2/*.*sh; do source \$f; done" >> $1
}

# This function creates a suitable profile file that sources
# the rc-file to ensure both are always executed.
init_profile() {
    echo > $1

    echo "for f in $DIR/sh/profile/pre/*.*sh; do source \$f; done" >> $1
    echo "for f in $DIR/sh/$3/profile/pre/*.*sh; do source \$f; done" >> $1

    if [ ! -z "$2" ]; then
        echo "if [ -f \"$2\" ]; then source $2; fi" >> $1
    fi

    # We source the profile scripts later
    # as they are usually added for special messages on login shells.
    echo "for f in $DIR/sh/profile/*.*sh; do source \$f; done" >> $1
    echo "for f in $DIR/sh/$3/profile/*.*sh; do source \$f; done" >> $1
}
