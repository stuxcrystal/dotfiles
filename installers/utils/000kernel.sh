action_desc() {
    printf "$1"
}

action_success() {
    local str
    str="$(printf "%*s" 4 "$1")"
    action_result $str 2
}

action_fail() {
    local str
    str="$(printf "%*s" 4 "$1")"
    action_result $str 1
}

action_result() {
    echoright "[$1]" "$(tput bold)[$(tput setaf $2)$1$(tput sgr0)$(tput bold)]$(tput sgr0)"
}

echoright() {
    __KERNEL_LENGTH="$(echo "$1" | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g')"
    __KERNEL_LENGTH=${#__KERNEL_LENGTH}

    getrow
    __KERNEL_ROW="$__KERNEL_RESULT"
    __KERNEL_COLUMN="$(($(tput cols)-$__KERNEL_LENGTH))"
    printf "$(tput cup $__KERNEL_ROW $__KERNEL_COLUMN)"
    printf "${2:-$1}"
}


getcolumn() {
    local oldstty
    exec < /dev/tty
    oldstty=$(stty -g)
    stty raw -echo min 0
    # on my system, the following line can be replaced by the line below it
    echo -en "\033[6n" > /dev/tty
    # tput u7 > /dev/tty    # when TERM=xterm (and relatives)
    IFS=';' read -r -d R -a pos
    stty $oldstty
    # change from one-based to zero based so they work with: tput cup $row $col
    __KERNEL_RESULT=$((${pos[1]} - 1))
}

getrow() {
    local oldstty
    exec < /dev/tty
    oldstty=$(stty -g)
    stty raw -echo min 0
    # on my system, the following line can be replaced by the line below it
    echo -en "\033[6n" > /dev/tty
    # tput u7 > /dev/tty    # when TERM=xterm (and relatives)
    IFS=';' read -r -d R -a pos
    stty $oldstty
    # change from one-based to zero based so they work with: tput cup $row $col
    __KERNEL_RESULT=$((${pos[0]:2} - 1))    # strip off the esc-[
}
