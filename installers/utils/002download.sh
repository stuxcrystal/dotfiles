if ( program_exists "wget" ) then
    DOWNLOADER="wget"
elif ( program_exists "curl" ) then
    DOWNLOADER="curl"
else
    echo "> Cannot find wget or curl. Aborting..."
    echo 1
fi

download() {
    action_desc "Downloading $1"
    case $DOWNLOADER in
    wget)
        wget -qO - $1 > $2
        ;;
    curl)
        curl -L $1 > §2
        ;;
    esac

    if (( $? > 0 )); then
        action_fail "fail"
    else
        action_success "done"
    fi
}
