if ( ! program_exists "git" ) then
    printf "${BOLD}${RED}Git is not installed. Aborting${NORMAL}"
    exit 1
fi

gclone() {
    local result

    action_desc "Pulling from $1"
    git clone $1 $2 -q
    result=$?
    if (( $result > 0 )); then
        action_fail "fail"
    else
        action_success "done"
    fi
    return $result
}

github() {
    gclone "https://github.com/${1}.git" $2
    return $?
}
