
init_shell ~/.zshrc zsh
init_profile ~/.zprofile ~/.zshrc zsh

download "https://raw.githubusercontent.com/zsh-users/antigen/master/antigen.zsh" $DIR/sh/zsh/lib/000antigen.zsh

action_desc "Initializing zsh"
zsh -c "source ~/.zshrc" 2>/dev/null >/dev/null
action_success "done"
