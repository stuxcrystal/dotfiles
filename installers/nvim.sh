mkdir -p ${XDG_CONFIG_HOME:=$HOME/.config} 2>/dev/null
ln -s $DIR/vim $XDG_CONFIG_HOME/nvim 2>/dev/null
ln -s $DIR/vim/start.vim $XDG_CONFIG_HOME/nvim/init.vim 2>/dev/null

nvim -c "source $DIR/vim/install.vim"
if [ -h $DIR/vim/vim ]; then rm $DIR/vim/vim; fi
