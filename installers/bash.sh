if [ -f ~/.bashrc ]; then rm ~/.bashrc; fi
if [ -f ~/.bash_prfile ]; then rm ~/.bash_profile; fi

if [ -d $DIR/sh/bash/lib/bashit ]; then
    rm -rf $DIR/sh/bash/lib/bashit
fi
mkdir $DIR/sh/bash/lib/bashit
gclone https://github.com/Bash-it/bash-it.git $DIR/sh/bash/lib/bashit -q

pushd $DIR/sh/bash/lib/bashit >/dev/null
cat ./install.sh | sed 's/read/# /g' | bash >/dev/null

echo "BASH_IT=$DIR/sh/bash/lib/bashit" > $DIR/sh/bash/lib/000bashit.sh
cat ~/.bashrc | grep source >> $DIR/sh/bash/lib/000bashit.sh
popd >/dev/null

init_shell ~/.bashrc bash
init_profile ~/.bash_profile ~/.bashrc bash
