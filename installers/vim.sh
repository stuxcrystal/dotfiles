
ln -s $DIR/vim ~/.vim 2>/dev/null
ln -s $DIR/vim/start.vim ~/.vimrc 2>/dev/null

vim -c "source $DIR/vim/install.vim"
if [ -h $DIR/vim/vim ]; then rm $DIR/vim/vim; fi
