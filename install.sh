#!/usr/bin/sh

#
# Resolve the current directory.
#
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#
# Does the program actually exist.
#
program_exists() {
    local f="$1"
    [[ -f "$(which $f 2>/dev/null)" ]] && return 0 || return 1
}

echo Initializing installer...
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)

echo Loading utlities...
for util in $DIR/installers/utils/*.sh; do
    source $util
done

echo Installing Programs...
for installer in $DIR/installers/*.sh; do
    __PROGRAM=$(basename $installer | rev | cut -c 4- | rev)
    if ( program_exists $__PROGRAM ) then
        printf "> $BOLD$GREEN$__PROGRAM$NORMAL$NORMALCOLOR\n"

        source $installer
    else
        printf "> $BOLD$RED$__PROGRAM$NORMAL$NORMALCOLOR\n"
    fi
done

