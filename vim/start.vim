call plug#begin("~/.vim/plugged")
source /home/stux/.vim/rc.vim

if has("nvim")
  source /home/stux/.vim/nvimrc.vim
else
  source /home/stux/.vim/svimrc.vim
end
call plug#end()
