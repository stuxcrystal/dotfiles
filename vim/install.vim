PlugInstall
if !has("nvim")
    qall
else
    update
    redraw!


    function! s:exit_install(jid, data, event)
        if s:first
            let s:first = 0
            wincmd l
            q
        end

        if a:event != "exit"
            return
        end

        let s:curline = getline(1)
        if s:curline =~ "^Updated."
            qall
            return
        end

        call s:restart_waiter()
    endfunction

    let s:first = 1
    let s:jobcb = { 'on_exit': function('s:exit_install'), 'name' : 'worlend' }

    function! s:restart_waiter()
        call jobstart(['bash', '-c', 'sleep 1'], s:jobcb)
    endfunction

    call s:restart_waiter()

end
