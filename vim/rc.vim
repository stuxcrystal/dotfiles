let non_android = 1
let python_support = has("python") || has("python3")
let windows = has("win32") || has("win16")

Plug 'bling/vim-airline'
if !windows
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
end

if python_support
    Plug 'davidhalter/jedi-vim', { 'for': 'python' }
end

noremap <Up> :echo "Stop being stupid"<CR>
noremap <Down> :echo "Stop being stupid"<CR>
noremap <Left> :echo "Stop being stupid"<CR>
noremap <Right> :echo "Stop being stupid"<CR>
noremap <C-X> <C-E>

Plug 'ervandew/supertab'
Plug 'Lokaltog/vim-easymotion'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tomtom/tcomment_vim'
Plug 'leafo/moonscript-vim'
Plug 'tpope/vim-fugitive'

runtime plugins/dragvisuals.vim

let mapleader=','
colorscheme mirodark

set ruler number relativenumber
set expandtab softtabstop=4 shiftwidth=4
set scrolloff=2
set colorcolumn=81
set autoread
set nowrap

hi CursorLine ctermbg=235
set cursorline!

set laststatus=2

let g:airline#extensions#tabline#enabled = 1
if !non_android
    let g:airline_left_sep = ''
    let g:airline_right_sep = ''
else
    let g:airline_powerline_fonts = 1
end
let g:airline_theme = 'zenburn'

set listchars=tab:>~,nbsp:_,trail:. list

cnoremap w!! w !sudo tee % > /dev/null
vnoremap < <gv
vnoremap > >gv

vmap  <expr>  <Left> DVB_Drag('left')
vmap  <expr>  <Right> DVB_Drag('right')
vmap  <expr>  <Down> DVB_Drag('down')
vmap  <expr>  <Up> DVB_Drag('up')
vmap  <expr>  D          DVB_Duplicate()

nmap <silent> <Leader>a :bn<CR>
nmap <silent> <Leader>s :bp<CR>
nmap <silent> <Leader>d :bd<CR>

nmap <silent> <Leader>f :FZF<CR>

nnoremap <Leader><Down> <S-DOWN>
nnoremap <Leader><Down> <S-UP>

filetype on
filetype plugin indent on

augroup Python
    autocmd!
    autocmd FileType python syntax enable
    autocmd FileType python setlocal completeopt-=preview
augroup END

augroup Java
    autocmd!
    autocmd FileType java syntax enable
    autocmd FileType java setlocal completeopt-=preview
augroup END

augroup Diff
    autocmd!
    autocmd filetype diff   syntax enable
augroup END

let g:SuperTabDefaultCompletionType = 'context'
