let $TERM="xterm-256colors"

tnoremap <Esc> <C-\><C-n>

" Extra Commands for the VIM Terminal.
command IPython te ipython
command PythonREPL te python
command Python3REPL te python3
